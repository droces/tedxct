/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {
  var map_init = false;
  var viewing_parties_data = null;

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.viewing_parties = {
  attach: function(context, settings) {
    // console.log('settings: ', settings);
    if (! map_init) {
      get_viewing_parties_data(context);
      map_init = true;
    }
  }
};


function get_viewing_parties_data(context) {
  var url = '/data/viewing-parties?_format=json';
  // var url = '/themes/txt/js/viewing_parties.json';
  $.get(url, function(data) {
    // console.log('data: ', data);
    viewing_parties_data = data;
    setup_map(context);
  });
}


function setup_map(context) {
  // console.log('setup_map()');
   if (! viewing_parties_data)
    return null;
  // console.log('viewing_parties_data: ', viewing_parties_data);

  // Create the map object
  // var map_container = $('#vp-map', context);
  var map = L.map('vp-map');
  var locations = [];

  // Add a tile layer
  // var server = 'http://{s}.tile.osm.org/{z}/{x}/{y}.png';
  var server = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
  var layer = L.tileLayer(server, {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  });
  layer.addTo(map);

  // Add markers
  $.each(viewing_parties_data, function(key, value) {
    // console.log(value);
    if (! value.location) {
      return null;
    }
    try {
      var location = JSON.parse('[' + value.location + ']');
    }
    catch (e) {
      console.log('Could not parse location');
      return null;
    }
    // console.log('location: ', location);

    var marker = L.marker(location);
    marker.addTo(map);
    locations.push(location);

    // Add a popup
    marker.bindPopup('<strong>' + value.title + '</strong>'
      + '<br>' + value.info_link);
    // marker.openPopup();
  });
  // console.log('locations: ', locations);

  // Set the map's view
  map.fitBounds(locations);
  map.zoomOut(1);
  if (locations.length === 1) {
    map.setZoom(11);
  }

  // Add a scale control
  var scale = L.control.scale()
  scale.addTo(map);
}


})(jQuery, Drupal, this, this.document);
