<?php

namespace Drupal\tedxname_development\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class DefaultController.
 *
 * @package Drupal\tedxname_development\Controller
 */
class DefaultController extends ControllerBase {

  /**
   * Testpage.
   *
   * @return string
   *   Return Hello string.
   */
  public function testPage() {
    // create_entities();

    // $node = node_load('1');
    // kint($node, '$node');
    update_node_data();

    $markup = '<p>Page info</p>';
    return [
      '#type' => 'markup',
      '#markup' => $markup,
    ];
  }

}
