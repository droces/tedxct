<?php

namespace Drupal\tedxname_core\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigManager;

/**
 * Provides a 'FeaturedEventBlock' block.
 *
 * @Block(
 *  id = "featured_event_block",
 *  admin_label = @Translation("Featured event block"),
 * )
 */
class FeaturedEventBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;
  /**
   * Constructs a new FeaturedEventBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        ConfigManager $config_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configManager = $config_manager;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
         'featured_event' => $this->t(''),
        ] + parent::defaultConfiguration();

 }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $featured_event = $this->configuration['featured_event'];
    $form['featured_event'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Featured event'),
      '#target_type' => 'node',
      '#selection_settings' => array(
        'target_bundles' => array('event'),
      ),
      '#description' => $this->t('The current upcoming or recent main TEDx event'),
      '#weight' => '0',
    ];

    if ($featured_event) {
      // kint($featured_event, '$featured_event');
      $form['featured_event']['#default_value'] = node_load($featured_event);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['featured_event'] = $form_state->getValue('featured_event');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $featured_event = $this->configuration['featured_event'];
    $event = node_load($featured_event);
    // kint($event, '$event');

    $venue = node_load($event->get('field_venue')->getValue()[0]['target_id']);
    // kint($venue, '$venue');

    $build['featured_event_block_featured_event'] = [
      '#theme' => 'featured_event',
      '#path' => $event->url(),
      '#title' => $event->getTitle(),
      '#date' => $event->get('field_date')->getValue()[0]['value'],
      '#venue' => $venue->getTitle(),
      '#subtitle' => $event->get('field_subtitle')->getValue()[0]['value'],
      '#sponsors' => [],
    ];

    $sponsors_val = $event->get('field_sponsors')->getValue();
    // kint($sponsors_val, '$sponsors_val');
    foreach ($sponsors_val as $key => $sponsor_val) {
      $sponsor = node_load($sponsor_val['target_id']);

      // Get the image URL
      $image_url = '';
      $image_id_val = $sponsor->get('field_image')->getValue();
      if ($image_id_val) {
        $image_id = $image_id_val[0]['target_id'];
        $image_file = file_load($image_id);
        $thumbnail_style = \Drupal::entityTypeManager()
          ->getStorage('image_style')->load('medium');
        $image_url = $thumbnail_style->buildUrl($image_file->getFileUri());
      }

      $build['featured_event_block_featured_event']['#sponsors'][] = [
        'title' => $sponsor->getTitle(),
        'path' => $sponsor->url(),
        'imageSrc' => $image_url,
      ];
    }

    return $build;
  }

}
