# Use the official Docker Library Drupal image
FROM drupal:8.5.2
# It contains Apache - the web server - and PHP.
# Both Apache and PHP are already setup for Drupal sites.

# Download and install needed CLI tools
RUN apt-get update && apt-get install -y \
	curl \
	git \
	mysql-client \
	nano \
	wget
# mysql-client is needed for Drush to function

# Download and install Composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
	php composer-setup.php && \
	mv composer.phar /usr/local/bin/composer && \
	php -r "unlink('composer-setup.php');"

# Download and install the Drush launcher
RUN wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.4.2/drush.phar && \
	chmod +x drush.phar && \
	mv drush.phar /usr/local/bin/drush

# Remove the provided Drupal files
RUN rm -rf /var/www/html/*

# # Copy project's Apache VirtualHost configuration file
# COPY apache-drupal.conf /etc/apache2/sites-enabled/000-default.conf

# Copy custom PHP settings for project
COPY drupal.ini /usr/local/etc/php/conf.d/drupal.ini
