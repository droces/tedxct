/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.txn = {
  attach: function(context, settings) {
    // console.log('context: ', context);

    set_up_carousel(context);

    addMenuToggle(context);

    $('body > .cover', context).click(function(event) {
      close_modals();
    });

    set_up_masonry('.masonry .view-content ul', context);
    set_up_masonry_section('.field--name-field-other-images', context);

    // swap_images_to_bg(context);

    set_up_event_page(context);

    $('#copy-email-signature', context).on('click', () => {
      const html = $('#email-signature', context)[0].outerHTML;
      // console.log('html:', html);
      copyToClipboard(html);
      alert('Copied.');
    });
  }
};


function set_up_narrow_menu(context) {
  // $('#show-menu', context).click(function(event) {
  //   // $('#block-txn-main-menu').attr('aria-hidden', 'false');
  //   Drupal.txn.toggleAttr($('#block-txn-main-menu'), 'aria-expanded', 'true');
  //   Drupal.txn.toggleAttr($('body > .cover'), 'aria-hidden', 'false');
  // });

  // Set up sub-menu toggle button
  // $('.menu li button[data-is-toggle]', context).click(function(event) {
  $('button#show-menu', context).click(function(event) {
    // $(this).parent().toggleClass('is-expanded');
    $('nav.menu--main').toggleClass('visible');
  });
}


function addMenuToggle(context) {
  let menuToggle = context.querySelector('#menu-toggle');
  if (! menuToggle)
    return;
  menuToggle.addEventListener('click', () => {
    // let menus = document.querySelectorAll('.region-header .block-menu, .region-nav .block-menu');
    let menus = document.querySelectorAll('nav.menu--main');

    menus.forEach((menu) => {
      menu.classList.toggle('visible');
      if (menu.classList.contains('visible')) {
        menuToggle.classList.add('expanded');
      }
      else {
        menuToggle.classList.remove('expanded');
      }
    });
  });
}


function set_up_carousel(context) {
  console.log('set_up_carousel()');

  $('.carousel, .field--name-field-slides', context).slick({
    autoplay: true,
    arrows: false,
    dots: true,
    fade: true,
    infinite: false
  });

  // Used on the Home page for Latest Talks
  $('.carousel-show4', context).slick({
    autoplay: true,
    arrows: false,
    dots: false,
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1
        }
      }
    ]
  });

  // Used on the Home page for Audience Quotes
  $('.has-carousel > *', context).slick({
    autoplay: true,
    arrows: true,
    dots: false,
    fade: true,
    infinite: true,
    prevArrow: '<button type="button" class="slick-prev"><span class="hidden">Previous</span>'
      + '<img src="/themes/txn/images/arrow-left.svg"></button>',
    nextArrow: '<button type="button" class="slick-next"><span class="hidden">Next</span>'
      + '<img src="/themes/txn/images/arrow-left.svg"></button>',
  });
}


// function swap_images_to_bg(context) {
//   $('.block.background-image', context).once('background-image').each(function(index, element) {
//     var image = $(this).find('.field-content img');
//     var src = image.attr('src');
//     // console.log('src: ', src);
//     $(this).css('background-image', "url('" + src + "')");
//     image.remove();
//   });
// }


function close_modals () {
  $('#block-txn-main-menu').attr('aria-expanded', 'false');
  $('body > .cover').attr('aria-hidden', 'true');
}


Drupal.txn = {
  toggleAttr: function(element, attr, value) {
    if (element.attr(attr) == value) {
      element.attr(attr, '');
    }
    else {
      element.attr(attr, value);
    }
  }
}

// Applies the masonry layout plugin to the artwork galleries
function set_up_masonry(container_selector) {
  var container = $(container_selector);

  container.imagesLoaded(function(){
    container.addClass('is-masonry-enabled');
    container.masonry({
      itemSelector: 'li',
      columnWidth: 'li' ,
      gutter: 0
    });
  });
}

// Applies the masonry layout plugin to the artwork galleries
function set_up_masonry_section(container_selector) {
  var container = $(container_selector);

  container.imagesLoaded(function(){
    container.addClass('is-masonry-enabled');
    container.masonry({
      itemSelector: 'div.field__item',
      columnWidth: 'div',
      gutter: 4
    });
  });
}


function set_up_event_page(context) {
  $('.tab-container [role="tab"]', context).click(function(event) {
    event.preventDefault();

    $(this).parents('[role="tablist"]').find('[role="tab"]')
      .attr('aria-selected', 'false');
    $(this).attr('aria-selected', 'true');

    var target = $(this).attr('aria-controls');
    // console.log('target: ', target);
    $(this).parents('main').find('[role="tabpanel"]')
      .attr('aria-hidden', 'true')
      .filter('#' + target)
      .attr('aria-hidden', 'false');
  });
}


// Copies a string to the clipboard. Must be called from within an
// event handler such as click. May return false if it failed, but
// this is not always possible. Browser support for Chrome 43+,
// Firefox 42+, Safari 10+, Edge and Internet Explorer 10+.
// Internet Explorer: The clipboard feature may be disabled by
// an administrator. By default a prompt is shown the first
// time the clipboard is used (per session).
// See https://stackoverflow.com/a/33928558/2702675
function copyToClipboard(text) {
  if (window.clipboardData && window.clipboardData.setData) {
    // Internet Explorer-specific code path to prevent textarea being shown while dialog is visible.
    return window.clipboardData.setData("Text", text);
  }
  else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
    var textarea = document.createElement("textarea");
    textarea.textContent = text;
    textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in Microsoft Edge.
    document.body.appendChild(textarea);
    textarea.select();
    try {
      return document.execCommand("copy");  // Security exception may be thrown by some browsers.
    }
    catch (ex) {
      console.warn("Copy to clipboard failed.", ex);
      return false;
    }
    finally {
      document.body.removeChild(textarea);
    }
  }
}



})(jQuery, Drupal, this, this.document);
